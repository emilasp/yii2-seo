<?php
namespace emilasp\seo\common\behaviors;

use emilasp\seo\common\models\SeoRedirect;
use emilasp\variety\models\Variety;
use yii;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\helpers\Inflector;
use emilasp\seo\common\models\Seo;
use emilasp\core\helpers\UrlHelper;
use emilasp\core\behaviors\base\DynamicAttributesBehavior;
use yii\helpers\Url;

/** Поведение добавляет моделе свойства seo и доступ к ним из связанной модели Seo
 *
 *  1) Добавляем в модель поведение
 *  public function behaviors()
 *  {
 *      return ArrayHelper::merge([
 *          [
 *              'class' => SeoModelBehavior::className(),
 *              'route' => '/module/controller/action', // роут до страницы модели
 *              'attributeName' => 'value', // атрибут из которого формируется slug
 *              'condition' => [    // условия для сохранения в seo table
 *                  'property' => [
 *                      'value_as_page' => true
 *                  ]
 *              ]
 *          ],
 *      ], parent::behaviors());
 *  }
 *
 *  2) Добавляем seo поля в rules
 *  [['title', 'title_h', 'keywords', 'description'], 'string', 'max' => 255],
 *
 *  3) Добавляем seo поля в форму
 *  <?= SeoForm::widget(['form' => $form, 'model' => $model]) ?>
 *
 * Class SeoModelBehavior
 * @package emilasp\seo\common\behaviors
 */
class SeoModelBehavior extends DynamicAttributesBehavior
{
    const TYPE_OBJECT_MODEL = 1;

    /** @var int тип связи */
    public $type = self::TYPE_OBJECT_MODEL;

    /** @var string имя атрибута */
    public $attributes = ['title', 'title_h', 'keywords', 'description'];

    /** @var string атрибут для генерации ссылки */
    public $attributeName = 'name';

    /** @var string атрибут для генерации уникальной ссылки */
    public $attributeUnique;


    /** @var array условия при котором мы работаем с сео */
    public $condition = [];

    /** @var string Роут для страницы модели */
    public $route;

    /** @var  null|Seo */
    private $model;

    /** @var array значения из формы */
    private $values = [];

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'saveSeoAttributes',
            ActiveRecord::EVENT_AFTER_UPDATE => 'saveSeoAttributes',
        ];
    }

    /** Устанавливаем значение поля из формы
     *
     * @param string $value
     */
    protected function setAttribute($name, $value)
    {
        $this->values[$name] = $value;
    }

    /** Получаем url для страницы модели
     *
     * @param bool $fullUrl - полный Url
     *
     * @return string
     */
    public function getUrl($title = false, $scheme = false)
    {
        $title = $title ?: $this->owner->{$this->attributeName};
        return self::getSeoUrl($this->route, $this->owner->id, $title, $scheme);
    }

    /** Собираем Url
     *
     * @param $route
     * @param $id
     * @param $title
     * @param bool $scheme
     *
     * @return string
     */
    public static function getSeoUrl($route, $id, $title, $scheme = false)
    {
        return Url::toRoute([
            $route,
            'id'    => $id,
            'title' => UrlHelper::strTourl($title),
        ], $scheme);
    }

    /**
     * Сохраняем модель Seo
     */
    public function saveSeoAttributes()
    {
        if ($this->type === self::TYPE_OBJECT_MODEL) {
            if ($this->checkConditions()) {
                $this->setModelSeo();

                $owner                    = $this->owner;
                $this->model->route       = $this->route;
                $this->model->object      = $owner::className();
                $this->model->object_id   = $owner->id;
                $this->model->title       = $this->getValue('title');
                $this->model->title_h     = $this->getValue('title_h');
                $this->model->keywords    = $this->getValue('keywords');
                $this->model->description = $this->getValue('description');
                $this->model->type        = Variety::getValue('seo_type_model');

                $name = $owner->{$this->attributeName};

                if (!$this->model->title) {
                    $this->model->title = $name;
                }
                if (!$this->model->title_h) {
                    $this->model->title_h = $name;
                }

                $this->model->updated_at = new Expression('NOW()');
                $this->model->save();
            }
        }
    }

    /** Получаем значение поля из seo модели
     * @return mixed
     */
    protected function getAttribute($name)
    {
        if ($this->type === self::TYPE_OBJECT_MODEL) {
            $this->setModelSeo();
            return $this->model->{$name};
        }
    }

    private function getValue($index)
    {
        return isset($this->values[$index]) ? $this->values[$index] : null;
    }

    /** Проверяем на соответствие условиям
     * @return bool
     */
    private function checkConditions()
    {
        if (count($this->condition) > 0) {
            foreach ($this->condition as $index => $cond) {
                if (is_array($cond)) {
                    foreach ($cond as $indexChild => $condChild) {
                        if ($this->owner->{$index}->{$indexChild} != $condChild) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * Устанавливаем модель seo для поведения
     * @TODO Попробовать добавить сюда вызов релейшена has one возможно будет джойнить
     */
    private function setModelSeo()
    {
        if (!$this->model) {
            $modelOwner = $this->owner;

            $this->model = Seo::findOne([
                'object'    => $modelOwner::className(),
                'object_id' => $modelOwner->id,
            ]);

            if (!$this->model) {
                $this->model = new Seo();
            }
        }
    }
}
