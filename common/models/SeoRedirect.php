<?php

namespace emilasp\seo\common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "seo_redirect".
 *
 * @property integer $id
 * @property string $hash
 * @property string $link
 * @property integer $seo_id
 * @property integer $counter
 * @property integer $type
 * @property string $created_at
 * @property string $updated_at
 */
class SeoRedirect extends \emilasp\core\components\base\ActiveRecord
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seo_redirect';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hash', 'link', 'seo_id'], 'required'],
            [['seo_id', 'counter', 'type'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['hash'], 'string', 'max' => 10],
            [['link'], 'string', 'max' => 255],
            [['link'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('seo', 'ID'),
            'hash'       => Yii::t('seo', 'Hash'),
            'link'       => Yii::t('seo', 'Link'),
            'seo_id'     => Yii::t('seo', 'Seo ID'),
            'counter'    => Yii::t('seo', 'Counter'),
            'type'       => Yii::t('seo', 'Type'),
            'created_at' => Yii::t('seo', 'Created At'),
            'updated_at' => Yii::t('seo', 'Updated At'),
        ];
    }

    /** Добавляем модель редирект
     * @param Seo $seoModel
     */
    public static function addRedirectModel(Seo $seoModel)
    {
        $redorectModel          = new self;
        $redorectModel->hash    = $seoModel->getOldAttribute('hash');
        $redorectModel->link    = $seoModel->getOldAttribute('link');
        $redorectModel->seo_id  = $seoModel->id;
        $redorectModel->type    = 1;
        $redorectModel->counter = 0;
        $redorectModel->save();
    }
}
