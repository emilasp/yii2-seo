<?php
namespace emilasp\seo\common\models;

use emilasp\core\helpers\StringHelper;
use emilasp\core\helpers\UrlHelper;
use emilasp\variety\behaviors\VarietyModelBehavior;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use emilasp\core\components\base\ActiveRecord;
use yii\helpers\Url;

/**
 * This is the model class for table "seo_seo".
 *
 * @property integer $id
 * @property string $hash
 * @property string $title
 * @property string $title_h
 * @property string $keywords
 * @property string $description
 * @property string $object
 * @property integer $object_id
 * @property string $route
 * @property string $params
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 */
class Seo extends ActiveRecord
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'variety_type' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'type',
                'group'     => 'seo_type',
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seo_seo';
    }

    /**
     * @inheritdoc
     */
    public function rules()

    {
        return [
            [['object_id', 'type'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'params', 'title_h', 'keywords', 'description'], 'string', 'max' => 255],
            [['object'], 'string', 'max' => 128],
            [['route'], 'string', 'max' => 100],
            [['hash'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('site', 'ID'),
            'hash'        => Yii::t('seo', 'Hash'),
            'title'       => Yii::t('seo', 'Title'),
            'title_h'     => Yii::t('seo', 'Title H'),
            'keywords'    => Yii::t('seo', 'Keywords'),
            'description' => Yii::t('seo', 'Description'),
            'object'      => Yii::t('site', 'Object'),
            'object_id'   => Yii::t('site', 'Object ID'),
            'redirect'    => Yii::t('seo', 'Redirect'),
            'route'       => Yii::t('seo', 'Route'),
            'params'      => Yii::t('site', 'Params'),
            'type'        => Yii::t('site', 'Type'),
            'created_at'  => Yii::t('site', 'Created At'),
            'updated_at'  => Yii::t('site', 'Updated At'),
        ];
    }
}
