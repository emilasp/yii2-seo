<?php

namespace emilasp\seo\common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use emilasp\seo\common\models\Seo;

/**
 * SeoSearch represents the model behind the search form about `emilasp\seo\common\models\Seo`.
 */
class SeoSearch extends Seo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'object_id', 'type'], 'integer'],
            [
                [
                    'hash',
                    'title',
                    'title_h',
                    'keywords',
                    'description',
                    'object',
                    'route',
                    'created_at',
                    'updated_at',
                ],
                'safe',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Seo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'         => $this->id,
            'object_id'  => $this->object_id,
            'type'       => $this->type,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'hash', $this->hash])
              ->andFilterWhere(['like', 'title', $this->title])
              ->andFilterWhere(['like', 'title_h', $this->title_h])
              ->andFilterWhere(['like', 'keywords', $this->keywords])
              ->andFilterWhere(['like', 'description', $this->description])
              ->andFilterWhere(['like', 'object', $this->object])
              ->andFilterWhere(['like', 'route', $this->route]);

        return $dataProvider;
    }
}
