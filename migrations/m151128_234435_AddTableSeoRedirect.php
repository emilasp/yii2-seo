<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m151128_234435_AddTableSeoRedirect extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('seo_redirect', [
            'id'         => $this->primaryKey(11),
            'hash'       => $this->string(10)->notNull(),
            'link'       => $this->string(255)->notNull()->unique(),
            'seo_id'     => $this->integer(11)->notNull(),
            'counter'    => $this->integer(11),
            'type'       => $this->smallInteger(1),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_seo_redirect',
            'seo_redirect',
            'seo_id',
            'seo_seo',
            'id'
        );

        $this->createIndex('seo_redirect_link', 'seo_redirect', ['hash', 'link']);
        $this->createIndex('seo_redirect_update', 'seo_redirect', ['updated_at']);

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('seo_redirect');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
