<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160402_145830_AddSeoTablesTag extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('seo_tag', [
            'id'         => $this->primaryKey(11),
            'name'       => $this->string(255)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $this->tableOptions);

        $this->createIndex('idx_seo_tag_name', 'seo_tag', 'name');

        $this->createTable('seo_tag_link', [
            'id'        => $this->primaryKey(11),
            'object'    => $this->string(255)->notNull(),
            'object_id' => $this->integer(11)->notNull(),
            'tag_id'    => $this->integer(11)->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_seo_tag_link_tag_id',
            'seo_tag_link',
            'tag_id',
            'seo_tag',
            'id'
        );

        $this->createIndex('seo_tag_link_object', 'seo_tag_link', ['object', 'object_id']);
        $this->createIndex('seo_tag_link_tag', 'seo_tag_link', ['tag_id']);

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('seo_tag_link');
        $this->dropTable('seo_tag');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
