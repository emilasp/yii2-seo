<?php

use emilasp\variety\models\Variety;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m151128_155608_AddSeoTable extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    public function up()
    {
        $this->createTable('seo_seo', [
            'id'          => $this->primaryKey(11),
            'hash'        => $this->string(10),
            'title'       => $this->string(255),
            'title_h'     => $this->string(255),
            'keywords'    => $this->string(255),
            'description' => $this->string(255),
            'object'      => $this->string(128),
            'object_id'   => $this->integer(11),
            'route'       => $this->string(100),
            'params'      => $this->string(200),
            'type'        => $this->smallInteger(1),
            'created_at'  => $this->dateTime(),
            'updated_at'  => $this->dateTime(),
        ], $this->tableOptions);

        $this->createIndex('seo_seo_route', 'seo_seo', ['route', 'object', 'object_id']);
        $this->createIndex('seo_seo_update', 'seo_seo', ['updated_at']);

        $this->addTypes();

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('seo_seo');

        $this->afterMigrate();
    }


    private function addTypes()
    {
        Variety::add('seo_type', 'seo_type_model', 'Модель', 1, 1);
        Variety::add('seo_type', 'seo_type_route', 'Роут', 2, 2);
        Variety::add('seo_type', 'seo_type_fictive', 'Фиктивный', 3, 3);
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
