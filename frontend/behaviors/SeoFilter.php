<?php
namespace emilasp\seo\frontend\behaviors;

use emilasp\im\common\models\Product;
use emilasp\seo\common\models\Seo;
use Yii;
use yii\base\Action;
use yii\base\ActionFilter;
use yii\di\Instance;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

/**
 * Class SeoFilter
 * @package frontend\modules\users\behaviors
 */
class SeoFilter extends ActionFilter
{
    public $actions = [
        'view',
        'index',
        'list',
    ];

    /**
     * Логируем разрешённые действия
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, $this->actions)) {
            $route = $this->getRoute();
            $id    = Yii::$app->request->get('id');

            $condition = ['route' => $route,];

            if ($id) {
                $condition['id'] = $id;
            }

            $seo = Seo::find()->where($condition)->one();

            Yii::$app->controller->seo = $seo;

            if ($seo) {
                $this->registerSeoTags(Yii::$app->controller, $seo);
            }
        }
        return true;
    }

    /** Получаем текущий роут
     * @return string
     */
    private function getRoute()
    {
        $module     = Yii::$app->controller->module->id;
        $controller = Yii::$app->controller->id;
        $action     = Yii::$app->controller->action->id;
        return '/' . $module . '/' . $controller . '/' . $action;
    }

    /** Регистрируем метатеги и основные html сео элементы
     * @param Controller $controller
     * @param Seo $seoModel
     */
    private function registerSeoTags(Controller &$controller, Seo $seoModel)
    {
        $controller->view->title = $seoModel->title;
        $controller->view->params['h1'] = $seoModel->title_h;
        $controller->view->registerMetaTag(['name' => 'keywords', 'content' => $seoModel->keywords]);
        $controller->view->registerMetaTag(['name' => 'description', 'content' => $seoModel->description]);
    }
}
