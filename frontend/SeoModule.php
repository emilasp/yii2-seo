<?php
namespace emilasp\seo\frontend;

use yii\helpers\ArrayHelper;
use emilasp\core\CoreModule;
use emilasp\settings\models\Setting;
use emilasp\settings\behaviors\SettingsBehavior;

/**
 * Class SeoModule
 * @package emilasp\seo\frontend
 */
class SeoModule extends CoreModule
{
    /** @var array карта роутов */
    public $routeMap = [];
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'setting' => [
                'class'    => SettingsBehavior::className(),
                'meta'     => [
                    'name' => 'Seo',
                    'type' => Setting::TYPE_MODULE,
                ],
                'settings' => [
                    [
                        'code'        => 'cache_page_duration',
                        'name'        => 'Кеширование страниц',
                        'description' => 'Время кеширования страниц',
                        'default' => 16,
                    ],
                ],
            ],
        ], parent::behaviors());
    }
}
