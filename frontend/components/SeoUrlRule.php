<?php
namespace emilasp\seo\frontend\components;

use yii\base\Object;
use yii\web\UrlRuleInterface;

/**
 * Class SeoUrlRule
 * @package app\components
 */
class SeoUrlRule extends Object implements UrlRuleInterface
{

    public $pattern = 'ss';
    public $route   = 'ss';

    public function createUrl($manager, $route, $params)
    {
        
    }


    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();
        if (preg_match('%^admin/(\w+)(/\d\w+)*$%', $pathInfo, $matches)) {
            $module  = $matches[1];
            $matches = array_slice($matches, 2);
            return [$module . '/admin' . implode('', $matches), [/*параметры*/]];
        }
        return false;
    }
}
