<?php

namespace emilasp\seo\frontend\controllers;

use emilasp\variety\models\Variety;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use emilasp\seo\common\models\Seo;
use emilasp\seo\common\models\SeoRedirect;
use emilasp\core\components\base\Controller;

/**
 * SeoController implements the CRUD actions for Seo model.
 */
class SeoController extends Controller
{
    public function behaviors()
    {
        return [
            /*[
                'class' => 'yii\filters\PageCache',
                'only' => ['view'],
                'duration' => $this->module->getSetting('cache_page_duration'),
                'variations' => [
                    Yii::$app->request->url,
                ],
                'dependency' => [
                    'class' => 'yii\caching\DbDependency',
                    'sql' => 'SELECT MAX(updated_at) FROM seo_seo',
                ],
            ],*/
        ];
    }

    /**
     * @param string $slug
     *
     * @throws NotFoundHttpException
     */
    public function actionIndex($slug = '')
    {
        $hash = Seo::generateHash($slug);
        /** @var Seo $seo */
        $seo =  Seo::findOne(['hash' => $hash, 'link' => $slug]);

        if (!$seo) {
            $this->checkRedirect($slug);
        }

        /*if (!isset($this->module->routeMap[$seo->object]['route'])) {
            $this->errorSlug();
        }*/

        list($controller) = Yii::$app->createController($seo->route);

        if (!$controller) {
            $this->errorSlug();
        }

        $this->registerSeoTags($controller, $seo);

        $action = $seo->getAction(true);
        if ($seo->type === Variety::getValue('seo_type_model')) {
            echo $controller->$action($seo->object_id);
        } else {
            echo call_user_func_array([$controller, $action], (array)$seo->params);
        }
    }

    /** Редиректим на нужный url если найдена запись в таблице redirect
     * @param $slug
     *
     * @throws NotFoundHttpException
     */
    private function checkRedirect($slug)
    {
        $seoRedirect =  SeoRedirect::findOne(['hash' => crc32($slug), 'link' => $slug]);

        if ($seoRedirect) {
            $seo =  Seo::findOne(['id' => $seoRedirect->seo_id]);
            if ($seo) {
                $seoRedirect->counter++;
                $seoRedirect->save();
                $this->redirect($seo::getUrl($seo->link), 301);
            }
        }
        $this->errorSlug();
    }

    /** Регистрируем метатеги и основные html сео элементы
     * @param Controller $controller
     * @param Seo $seoModel
     */
    private function registerSeoTags(Controller &$controller, Seo $seoModel)
    {
        $controller->view->title = $seoModel->title;
        $controller->view->params['h1'] = $seoModel->title_h;
        $controller->view->registerMetaTag(['name' => 'keywords', 'content' => $seoModel->keywords]);
        $controller->view->registerMetaTag(['name' => 'description', 'content' => $seoModel->description]);
    }

    /** выкидываем исключение
     * @throws NotFoundHttpException
     */
    private function errorSlug()
    {
        throw new NotFoundHttpException(Yii::t('site', 'Page not found'));
    }
}
