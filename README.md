Модуль SEO для Yii2
=============================

Модуль для реализации seo дружественн

Installation
------------

Добавление нового модуля

Add to composer.json:
```json
"emilasp/yii2-seo": "*"
```
AND
```json
"repositories": [
        {
            "type": "git",
            "url":  "https://bitbucket.org/emilasp/yii2-seo.git"
        }
    ]
```

to the require section of your `composer.json` file.

Configuring
--------------------------

Бек

Добавляем в конфиг модуль:
```php
'seo' => [
    'class'     => 'emilasp\seo\backend\SeoModule',
],
```

Добавляем в форму модели вывод полей:
```php
echo SeoForm::widget(['form' => $form, 'model' => $model]);
```

Добавляем поведение модели:
```php
public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class'     => SeoModelBehavior::className(),
            ],
        ], parent::behaviors());
    }
```


Фронт

Добавляем в конфиг фронта:
```php
'seo' => [
    'class'     => 'emilasp\seo\frontend\SeoModule',
    'routeMap' => [
        \emilasp\site\common\models\Page::className() => [
            'route' => '/site/page/view'
        ],
    ],
],
```

Добавляем в UrlManager правило:
```php
 '<slug:[a-zA-Z0-9_-]+>.html'  => 'seo/seo/view',
```

