<?php
namespace emilasp\seo\backend\widgets\SeoForm;

use yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/**
 * Выводим элементы формы отвечающие за seo
 *
 * Class SeoForm
 * @package emilasp\seo\backend\widgets\SeoForm
 */
class SeoForm extends \yii\base\Widget
{
    public $model;
    public $form;

    public function run()
    {
        echo $this->render('form', ['form' => $this->form, 'model' => $this->model]);
    }
}
