<?= $form->field($model, 'title')->textInput(['maxlength' => true])->label(Yii::t('seo', 'Title')) ?>

<?= $form->field($model, 'title_h')->textInput(['maxlength' => true])->label(Yii::t('seo', 'Title H')) ?>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'keywords')->textarea(['rows' => 3])->label(Yii::t('seo', 'Keywords')) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'description')->textarea(['rows' => 3])->label(Yii::t('seo', 'Description')) ?>
    </div>
</div>