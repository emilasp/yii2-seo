<?php
namespace emilasp\seo\backend;

use emilasp\core\CoreModule;

/**
 * Class SeoModule
 * @package emilasp\seo\backend
 */
class SeoModule extends \emilasp\seo\frontend\SeoModule
{
    public $pathModules = [];
}
