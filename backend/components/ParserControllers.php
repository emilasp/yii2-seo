<?php

namespace emilasp\seo\backend\components;

use DirectoryIterator;
use emilasp\seo\common\models\Seo;
use emilasp\variety\models\Variety;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Парсер контроллеров
 *
 * Class ParserControllers
 * @package emilasp\seo\backend\components
 */
class ParserControllers extends Component
{
    const ACTION_TAG_START = 'function[ ]*action';
    const ACTION_TAG_END   = '\(';

    const COMMENT_TAG_START = '\/\*\*.+?function[ ]{1,5}action[w]*';
    const COMMENT_TAG_END = '[(].';

    public $modules = [];

    /**
     * INIT
     */
    public function init()
    {

        parent::init();
    }

    /**
     * Запускаем парсер
     */
    public function run()
    {
        $this->parseModules();
    }

    /** Получаем все файлы в директории
     *
     * @param $path
     *
     * @return array
     */
    private function getFiles($path)
    {
        $files = [];
        foreach (new DirectoryIterator($path) as $file) {
            if (!$file->isFile()) {
                continue;
            }
            $files[$file->getFilename()] = $file->getPath() . DIRECTORY_SEPARATOR . $file->getFilename();
        }
        return $files;
    }

    /**
     * Парсим модуля проекта
     */
    private function parseModules()
    {

        $typesSeo = array_flip(Variety::getVariety('seo_type'));

        foreach (Yii::$app->getModule('seo')->pathModules as $name => $path) {
            $path  = Yii::getAlias($path . DIRECTORY_SEPARATOR . 'controllers');
            $files = $this->getFiles($path);

            foreach ($files as $controller => $file) {
                if (!$this->isController($file)) {
                    continue;
                }

                $comments = $this->parseContent( $file, self::COMMENT_TAG_START, self::COMMENT_TAG_END );

                $actions = $this->parseContent(
                    $file,
                    self::ACTION_TAG_START,
                    self::ACTION_TAG_END,
                    1
                );

                foreach ($actions as $index => $action) {
                    $controller = strtolower(str_replace('Controller.php', '', $controller));
                    $route = '/' . $name . '/' . $controller . '/' . $action;

                    $newEl = [];
                    $newEl['action'] = $action;
                    $newEl['description'] = (!empty($comments[0][$index]) && strpos($comments[0][$index], $action))
                        ? $comments[0][$index] : '';
                    $seo = Seo::find()
                        ->where(['route' => $route])->one();

                    if ($seo) {
                        $newEl['id'] = $seo->id;
                        $newEl['type'] = $typesSeo[$seo->type];
                        $newEl['route'] = $seo->route;
                    } else {
                        $newEl['id'] = null;
                        $newEl['type'] = null;
                        $newEl['route'] = $route;
                    }


                    $this->modules[$name][$controller][] = $newEl;
                }


                /*$this->modules[$name][$controller] = $this->parseContent(
                    $file,
                    self::ACTION_TAG_START,
                    self::ACTION_TAG_END,
                    1
                );*/
            }
        }
    }

    /** Удостоверяемся что файл - контроллер
     *
     * @param string $filePath
     *
     * @return bool
     */
    private function isController($filePath)
    {
        return strpos($filePath, 'Controller') !== false;
    }


    /** Парсим файл/текст регуляркой
     *
     * @param string $path
     * @param string $start
     * @param string $end
     * @param int $returnIndex
     * @param string $search
     *
     * @return mixed
     */
    public function parseContent($path, $start, $end, $returnIndex = null, $search = '(.+?)')
    {

        if (is_array($path)) {
            $code = $path['content'];
        } else {
            $code = file_get_contents($path);
        }

        $pattern = '#' . $start . $search . $end . '#is';
        preg_match_all($pattern, $code, $matches);

        if ($returnIndex === null) {
            return $matches;
        }
        return $matches[$returnIndex];
    }
}
