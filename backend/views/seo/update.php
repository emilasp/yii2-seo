<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\seo\common\models\Seo */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('seo', 'Seo'),
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('seo', 'Seos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Update');
?>
<div class="seo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
