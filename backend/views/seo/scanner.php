<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\seo\common\models\SeoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('seo', 'Scanner');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-index">

    <div class="row">
        <div class="col-md-3">
            <ul class="nav nav-pills nav-stacked">
                <?php $firstIter = true; ?>
                <?php foreach ($modules as $moduleName => $controllers) : ?>
                    <li <?= ($firstIter) ? 'class="active"' : '' ?>>
                        <a data-toggle="tab" href="#<?= $moduleName ?>"><?= $moduleName ?></a>
                    </li>
                    <?php $firstIter = false; ?>
                <?php endforeach ?>
            </ul>
        </div>

        <div class="col-md-9">
            <div class="tab-content">

                <?php $firstIter = true; ?>
                <?php foreach ($modules as $moduleName => $controllers) : ?>
                    <div id="<?= $moduleName ?>" class="tab-pane fade <?= ($firstIter) ? ' in active' : '' ?>">


                        <?php foreach ($controllers as $controllerName => $actions) : ?>

                            <?php
                             $dataProvider = new ArrayDataProvider([
                                 'allModels' => $actions,
                                 'pagination' => [
                                     'pageSize' => 1000,
                                 ],
                                 'sort' => [
                                     'attributes' => ['action', 'bd'],
                                 ],
                             ]);
                            ?>

                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                //'filterModel'  => $searchModel,
                                'columns'      => [
                                    ['class' => '\kartik\grid\SerialColumn'],
                                    [
                                        'attribute' => 'action',
                                        'class'     => '\kartik\grid\DataColumn',
                                        /*'value'     => function ($model, $key, $index, $column) {
                                            return $model->statuses[$model->status];
                                        },*/
                                        'hAlign'    => GridView::ALIGN_LEFT,
                                        'vAlign'    => GridView::ALIGN_MIDDLE,
                                    ],
                                    [
                                        'attribute' => 'description',
                                        'class'     => '\kartik\grid\DataColumn',
                                        'hAlign'    => GridView::ALIGN_LEFT,
                                        'vAlign'    => GridView::ALIGN_MIDDLE,
                                    ],
                                    'id',
                                    'type',
                                    [
                                        'attribute' => 'actions',
                                        'class'     => '\kartik\grid\DataColumn',
                                        'value'     => function ($model, $key, $index, $column) {
                                            $html = '';
                                            if ($model['id']) {
                                                $html .= Html::a(
                                                    '<i class="fa fa-pencil"></i>&nbsp;',
                                                    ['/seo/seo/update', 'id' => $model['id']],
                                                    ['target' => '_blank']
                                                );
                                                $html .= Html::a(
                                                    '<i class="fa fa-trash"></i>',
                                                    ['/seo/seo/delete', 'id' => $model['id']],
                                                    [
                                                        'title' => '/Удалить',
                                                        'data'  => [
                                                            'confirm' => 'Удалить данную запись?',
                                                            'method'  => 'post',
                                                            'pjax'    => '0',
                                                        ],
                                                    ]
                                                );
                                            } else {
                                                $html .= Html::a(
                                                    '<i class="fa fa-plus"></i>',
                                                    ['/seo/seo/create', 'route' => $model['route']],
                                                    ['target' => '_blank']
                                                );
                                            }

                                            return $html;
                                        },
                                        'hAlign'    => GridView::ALIGN_LEFT,
                                        'vAlign'    => GridView::ALIGN_MIDDLE,
                                        'format'    => 'raw',
                                    ],
                                ],
                                'responsive'   => true,
                                'hover'        => true,
                                'condensed'    => true,
                            ]);
                            ?>

                        <?php endforeach ?>

                    </div>
                    <?php $firstIter = false; ?>
                <?php endforeach ?>


            </div>
        </div>
    </div>



</div>
